import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/models/user.dart';

import './edit_user_screen.dart';


class UserListScreen extends StatefulWidget {
	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
	Future? futureUsers;
    final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    
    
    @override
	void initState() {
		super.initState();

        //kapag naload na
		WidgetsBinding.instance!.addPostFrameCallback((timestamp){
			setState(() {
			  futureUsers = API().getUsers();
			});
		});
	}


    List<Widget> generateListTiles(List<User> users) {
		List<Widget> listTiles = [];

		for (User user in users) {
			listTiles.add(Container(
                child: Column(
                    children: [
                        Card(
                            child: Column( 
                                children:[
                                    ListTile(
                                        title: Text(user.name),
                                        subtitle: Text(user.email),
                                        trailing: IconButton(
                                                onPressed: (){
                                                    Navigator.push(context, MaterialPageRoute(builder: (context) => EditScreen(user)));
                                                },
                                                icon: Icon(Icons.edit)
                                            ) 
                                    )
                                ]  
                                 
                                
                            )   
                        )
                    ]   
                )
                
            ));
        
		}
		return listTiles;
	}

    void _didPushButton() {
        Navigator.pushNamed(context, '/adduser');
    }

	@override
	Widget build(BuildContext context) {
        //pag may nagbabago na parang once nag update yung future users nagbaabago din siya
        Widget fbUserList = FutureBuilder(
			future: futureUsers,
			builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done){
                    if(snapshot.hasError){
                        return Center(
                            child: Text('Could not load the user list, Restart the app')
                        );
                    } 
                    
                    return RefreshIndicator(
						key: refreshIndicatorKey,
						onRefresh: () async {
							setState(() {
								futureUsers = API().getUsers();
							});
						},
						child: ListView(
							padding: EdgeInsets.all(8.0),
							children: generateListTiles(snapshot.data as List<User>)
						)
					);
                }

                return Center(
                    child: CircularProgressIndicator()
                );

				// print(snapshot.connectionState);
                // print(snapshot.hasData);
                // print(snapshot.hasError);
                // print(snapshot.data);
                // return Text('You are in the user list screen.');
			}
		);

		return Scaffold(
			appBar: AppBar(title: Text('User List')),
            drawer: Drawer(
                // Add a ListView to the drawer. This ensures the user can scroll
                // through the options in the drawer if there isn't enough vertical
                // space to fit everything.
                child: ListView(
                // Important: Remove any padding from the ListView.
                    padding: EdgeInsets.zero,
                    children: [
                        const DrawerHeader(
                        decoration: BoxDecoration(
                            color: Colors.blue,
                        ),
                        child: Text('HELLO'),
                        ),
                        ListTile(
                            title: const Text('Add User'),
                            onTap: () {
                                _didPushButton();
                            },
                        ),
                    ],
                ),
            ),
			body: Container(
                child: fbUserList
            )
		);
	}
}