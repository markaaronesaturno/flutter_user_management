import 'package:flutter/material.dart';
import 'package:flutter_user_management/models/user.dart';
import 'dart:async';


import '/utils/api.dart';

class EditScreen extends StatefulWidget {
    final User? user;

    EditScreen(this.user);

    @override
    _EditScreenState createState() => _EditScreenState();

}

class _EditScreenState extends State<EditScreen> {
    Future?_futureUser;

    

    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffNameController = TextEditingController();
    final _tffPhoneController = TextEditingController();
    final _tffWebsiteController = TextEditingController();
  

    void editUser(BuildContext context){
        setState(() {
            _futureUser = API().updateUser(
                    id : widget.user!.id,
                    name: _tffNameController.text,
                    email: _tffEmailController.text, 
                    phone: _tffPhoneController.text,
                    website: _tffWebsiteController.text
            ); 
        });
          
    }

    void showSnackBar(BuildContext context, String message) {
        SnackBar snackBar = new SnackBar(
            content: Text(message), 
            duration: Duration(milliseconds: 2000) 
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }  
    
  
  @override
  Widget build(BuildContext context) {


    Widget tffName = TextFormField(
            decoration: InputDecoration(
                labelText: 'Name', 
            ),
            keyboardType: TextInputType.name,
            controller: _tffNameController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The name must be provided';
            },
        );

        Widget tffEmail = TextFormField(
            decoration: InputDecoration(
                labelText: 'Email', 
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
            },
        );

        Widget tffPhone = TextFormField(
            decoration: InputDecoration(
                labelText: 'Phone', 
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffPhoneController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The phone must be provided';
            },
        );

        Widget tffWebsite = TextFormField(
            decoration: InputDecoration(
                labelText: 'Website', 
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffWebsiteController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The website must be provided';
            },
        );


        Widget btnRegister = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Edit'),
                onPressed: () {
                    if(_formKey.currentState!.validate()){
                        editUser(context); 
                    } else {
                        showSnackBar(context, 'Update registration fields to pass all validations.');
                    }
                }
            )
        );

        Widget formRegister = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        tffName,
                        tffEmail,
                        tffPhone,
                        tffWebsite,
                        btnRegister,
                        
                    ]
                )
            )
        );

        Widget registerView = FutureBuilder(
            future: _futureUser,
            builder: (context, snapshot){
                print(snapshot.data);
                if(_futureUser == null){
                    return formRegister;
                } else if(snapshot.hasData){
                    //lagay yung import 'dart:async';
                        print(snapshot.data);
                        Timer(Duration(seconds: 3), (){
                        //pushReplacementNamed LoginScreen <- LoginScreen
                        //pushNamedAndRemoveUnti = LoginScree -> RegisterScreen then mawawala lahat to -> LoginScreen 
                            Navigator.pushNamed(context, '/');
                        });
                     return Column(
                        children: [
                            Text('Registration successful, You will be redirected shortly back to the login page.'),
                        ]
                    );   
                    
                }
                else {
                    return Center(
                        child: CircularProgressIndicator()
                    );
                    
                }

            }
        );
    
    return Scaffold(
        appBar: AppBar(title: Text('Edit user')),
        body: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(16.0),
                    child: registerView
                )
            );
    }
  
}
