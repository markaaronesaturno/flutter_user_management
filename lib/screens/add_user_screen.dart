import 'package:flutter/material.dart';

import '/utils/api.dart';

class AddUserScreen extends StatefulWidget {
  @override
  _AddUserScreenState createState() => _AddUserScreenState();
}

class _AddUserScreenState extends State<AddUserScreen> {
    Future?_futureUser;


    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffNameController = TextEditingController();
    final _tffPhoneController = TextEditingController();
    final _tffWebsiteController = TextEditingController();
    

    void addUser(BuildContext context){
        setState(() {
            _futureUser = API().addUser(
                    name: _tffNameController.text,
                    email: _tffEmailController.text, 
                    phone: _tffPhoneController.text,
                    website: _tffWebsiteController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            }); 
        });
          
    }

     void showSnackBar(BuildContext context, String message) {
        SnackBar snackBar = new SnackBar(
            content: Text(message), 
            duration: Duration(milliseconds: 2000) 
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }  


    @override
    Widget build(BuildContext context) {
      
        Widget tffName = TextFormField(
            decoration: InputDecoration(
                labelText: 'Name', 
            ),
            keyboardType: TextInputType.name,
            controller: _tffNameController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The name must be provided';
            },
        );

        Widget tffEmail = TextFormField(
            decoration: InputDecoration(
                labelText: 'Email', 
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
            },
        );

        Widget tffPhone = TextFormField(
            decoration: InputDecoration(
                labelText: 'Phone', 
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffPhoneController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The phone must be provided';
            },
        );

        Widget tffWebsite = TextFormField(
            decoration: InputDecoration(
                labelText: 'Website', 
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffWebsiteController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The website must be provided';
            },
        );


        Widget btnRegister = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Add user'),
                onPressed: () {
                    if(_formKey.currentState!.validate()){
                        
                        addUser(context);
                        Navigator.pushReplacementNamed(context, '/');
                        showSnackBar(context, 'Successfully added User.');
                    
                    } else {
                        showSnackBar(context, 'Complete the required fields');
                    }
                }
            )
        );

        Widget formRegister = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        tffName,
                        tffEmail,
                        tffPhone,
                        tffWebsite,
                        btnRegister,    
                    ]
                )
            )
        );

        Widget registerView = FutureBuilder(
            future: _futureUser,
            builder: (context, snapshot){
                if(_futureUser == null){
                    return formRegister;
                } else if(snapshot.hasData && snapshot.data == true){
                    //lagay yung import 'dart:async';
                    
                        //pushReplacementNamed LoginScreen <- LoginScreen
                        //pushNamedAndRemoveUnti = LoginScree -> RegisterScreen then mawawala lahat to -> LoginScreen 
                        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
            
                    
                        return Column(
                            children: [
                                Text('User added successful, You will be redirected shortly back to the login page.'),
                        ]
                    );
                } else {
                    return Column(
                        children: [
                            Text('Error the Id is not in the data'),
                        ]
                    );
                    
                }
            }
        );
    
    return Scaffold(
        appBar: AppBar(title: Text('Add a user')),
        body: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(16.0),
                    child: registerView
                )
            );
    }
}