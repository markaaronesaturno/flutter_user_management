import 'package:dio/dio.dart';

import '/models/user.dart';

class API {
	Future getUsers() async {
		try {
			String url = 'https://jsonplaceholder.typicode.com/users';
			var response = await Dio().get(url);
			List<User> users= [];

            for(var user in response.data){
                // The 'user' here is of Map<String, dynamic> type
                // The 'user; map is converted into a User object
                // After conversion, the object is added to the list
                users.add(User.fromJson(user));
            }
            // print(response.data[0]);
            // print(users[0]);
            return users;

		} catch (exception) {
			throw(exception);
		}
	}

    
    Future addUser({
        required String name,
        required String email,
        required String phone,
        required String website     
    })async {    
        try{
            String url = 'https://jsonplaceholder.typicode.com/users';
            var response = await Dio().post(
                url,
                data: {
                    'name' : name,
                    'email' : email,
                    'phone' : phone,
                    'website' : website  
                }
            );
            print(response.data);
            return(response.data);
        
        } catch (exception) {
            throw(exception);
        }
    }


    Future? updateUser({
        required int? id,
        required String name,
        required String email,
        required String phone,
        required String website  
        
        }) async {
            User? updatedUser;
        try {
            String url = 'https://jsonplaceholder.typicode.com/users';
            var response = await Dio().put(
                url + '/$id',
                data: {
                'name' : name,
                'email' : email,
                'phone' : phone,
                'website' : website  
                }
            );

            print('User updated: ${response.data}');
            return(response.data);
        
        } catch (e) {
            print('Error updating user: $e');
        }

        return updatedUser;
    }
}